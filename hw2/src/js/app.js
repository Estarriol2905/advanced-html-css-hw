const burgerBtn = document.querySelector('.burger-btn');
const burgerBtnImg = document.querySelector('.burger-btn__img');
const menu = document.querySelector('.page-header__menu');
burgerBtn.onclick = (event) =>{
    menu.classList.toggle('page-header__menu--active');
    if (burgerBtn.classList.contains('burger-btn--active')){
        burgerBtn.classList.remove('burger-btn--active');
        burgerBtnImg.previousElementSibling.srcset = "./img/burger-menu.webp";
        burgerBtnImg.src = "./img/burger-menu.png";
    } else {
        burgerBtn.classList.add('burger-btn--active');
        burgerBtnImg.previousElementSibling.srcset = "./img/burger-menu-active.webp";
        burgerBtnImg.src = "./img/burger-menu-active.png";
    }
}
