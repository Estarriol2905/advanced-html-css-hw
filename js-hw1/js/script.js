class Employee {
    constructor(name, age, salary) {
      this._name = name;
      this._age = age;
      this._salary = salary;
    }

    get name() {
        return this._name;
      }
    set name(value) {
        if (value.length < 3) {
          alert("Ім'я надто коротке.");
          return;
        }
        this._name = value;
    }

    get age() {
        return this._age;
      }
    set age(value) {
        if (value < 1) {
          alert("Введіть коректний вік");
          return;
        }
        this._age = value;
    }
    get salary() {
        return this._salary;
      }
    set salary(value) {
        if (value <= 0) {
          alert("З/п повинна бути більше за нуль");
          return;
        }
        this._salary = value;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
      this._lang = lang;
    }
    
    get salary() {
        return this._salary * 3;
      }
    set salary(value) {
        if (value <= 0) {
          alert("З/п повинна бути більше за нуль");
          return;
        }
        this._salary = value;
    }
}


let prog1 = new Programmer (
    'David',
    18,
    1000,
    'Spanish'
);

console.log(prog1);
console.log(prog1.salary);

let prog2 = new Programmer (
    'Kate',
    20,
    2000,
    'English'
);

console.log(prog2);
console.log(prog2.salary);