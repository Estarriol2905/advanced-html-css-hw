"use strict";
const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
   author: "Сюзанна Кларк",
   name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
   author: "Террі Пратчетт",
   name: "Рухомі картинки",
   price: 40
  },
  {
   author: "Анґус Гайленд",
   name: "Коти в мистецтві",
  }
];

const root = document.getElementById("root");
const ul = document.createElement('ul');
root.insertAdjacentElement('afterbegin', ul);

books.forEach((obj) => {
  try {
    if (!obj.author){
        throw new Error('Не має автора книги');
    }
    if (!obj.name){
        throw new Error('Не має назви книги');
    }
    if (!obj.price){
        throw new Error('Не має ціни книги');
    }
    const keys = Object.keys(obj);
    const list = document.createElement('li');
    list.style = "margin-bottom: 10px;";
    keys.forEach(key => {
      const li = document.createElement('li');
      li.style = "list-style: none;"
      li.textContent = `${key} : ${obj[key]}`;
      list.insertAdjacentElement("beforeend", li);
    });
    ul.insertAdjacentElement("beforeend", list);
}
catch (error) {
  console.log(error.message);
}
});