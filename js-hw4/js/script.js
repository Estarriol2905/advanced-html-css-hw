"use strict";
const root = document.getElementById("root");
const url = 'https://ajax.test-danit.com/api/swapi/films';

class Films {
  constructor(url, root) {
    this.url = url;
    this.root = root;
  }

  getFilms(url){
    return fetch(url, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      },
    }).then(response => {
      return response.json();
    }).catch(error => {
      return console.error(error)
    })
  }

  render() {
    this.getFilms(url).then((data) => {
      let filmsList = document.createElement('ol');
      data.forEach(({episodeId, name, openingCrawl, characters}) => {
      let filmsData = document.createElement('li');
      filmsData.innerHTML = '<span class="accent-font">Episode ' + episodeId + '. ' + name + '. </span></br>' + openingCrawl;
      filmsList.append(filmsData);
      let charactersArray = [];
        characters.forEach((data) => {
        charactersArray.push(this.getFilms(data));
        })
        Promise.all(charactersArray)
        .then(data => {
          const charactersFilms = document.createElement('div');
          charactersFilms.innerHTML = '<span class="accent-font">Characters:</span></br>';
            data.forEach(({name}) => {
              return charactersFilms.innerHTML += name + ', ';
            });
            filmsData.append(charactersFilms);
        })
      })
      this.root.append(filmsList);
    })
  }
}

const films = new Films(url, root);
films.render();