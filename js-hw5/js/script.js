"use strict";
const root = document.getElementById("root");

class Card {
  constructor(title, text, user, email, postId) {
      this.title = title;
      this.text = text;
      this.user = user;
      this.email = email;
      this.postId = postId;
  }

  template = document.getElementById('template').content;

  render(root) {
      this.card = this.template.querySelector('.card').cloneNode(true);
      this.cardTitle = this.card.querySelector('.title');
      this.cardText = this.card.querySelector('.text');
      const user = this.card.querySelector('.user');
      const email = this.card.querySelector('.user-email');

      this.cardTitle.textContent = this.title;
      this.cardText.textContent = this.text;
      user.textContent = this.user;
      email.textContent = this.email;

      this.card.dataset.id = this.postId;
      root.append(this.card);
  }

  delete(id) {
      const deleteButton = this.card.querySelector('.button-delete');
      deleteButton.addEventListener('click', (e) => {
          fetch("https://ajax.test-danit.com/api/json/posts/" + id, {method: 'DELETE'})
              .then(response => {
                e.target.closest('.card').remove();
              })
              .catch(error => console.log(error));
      })
  }
}

class List {
  constructor(root) {
      this.root = root
  }

  getData(url) {
    return fetch(url)
        .then(response => response.json())
        .catch(error => console.error(error))
  }

  render() {
      const users = this.getData('https://ajax.test-danit.com/api/json/users');
      const posts = this.getData('https://ajax.test-danit.com/api/json/posts');
      Promise.all([users, posts])
          .then(data => {
              this.users = data[0].map(({id, name, email}) => ({id, name, email}));
              this.posts = data[1].map(({id, userId, title, body}) => ({id, userId, title, body}));
              this.posts.forEach(({id: postId, userId, title, body}) => {
                  const {name, email} = this.users.find(({id}) => id === userId);
                  const card = new Card(title, body, name, email, postId);
                  card.render(this.root);
                  card.delete(postId);
              })
          })
  }
}

const list = new List(root);
list.render();