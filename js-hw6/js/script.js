"use strict";
const btn = document.getElementById("btn");

async function getIp(){
  let response = await fetch('https://api.ipify.org/?format=json');
  let {ip} = await response.json();
  return ip;
}
async function getInfo(button){
  let query = await getIp();
  let response = await fetch(`http://ip-api.com/json/${query}?lang=ru&fields=status,message,continent,country,regionName,city,district`);
  let {continent, country, regionName, city, district} = await response.json();
  if (district === ''){
    district = city;
  };
  button.insertAdjacentHTML('afterend', `<p>Континет: ${continent}.</br>Cтрана: ${country}.</br> Регион: ${regionName}.</br>Город: ${city}.</br> Район: ${district}</p>`);
}

btn.addEventListener("click", () => {
  getInfo(btn);
});